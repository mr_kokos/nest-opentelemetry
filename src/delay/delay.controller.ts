import { Controller, Get } from '@nestjs/common';
import { DelayService } from './delay.service';
@Controller('delay')
export class DelayController {
  constructor(private delayService: DelayService) {}
  @Get('short')
  shortDelay(): Promise<string> {
    return new Promise((resolve) => {
      setTimeout(() => resolve('Short delay completed'), 3000);
    });
  }

  @Get('medium')
  mediumDelay(): Promise<string> {
    return new Promise((resolve) => {
      setTimeout(() => resolve(this.delayService.superDelay()), 6000);
    });
  }

  @Get('long')
  longDelay(): Promise<string> {
    return new Promise((resolve) => {
      setTimeout(() => resolve('Long delay completed'), 8000);
    });
  }
}