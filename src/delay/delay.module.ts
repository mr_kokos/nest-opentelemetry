import { Module } from '@nestjs/common';
import { DelayController } from './delay.controller';
import { DelayService } from './delay.service';

@Module({
  controllers: [DelayController],
  providers: [DelayService]
})
export class DelayModule {}
