import { Injectable } from '@nestjs/common';

@Injectable()
export class DelayService {
    superDelay():  Promise<string> {        
        return new Promise((resolve) => {
            setTimeout(() => resolve('Medium delay completed'), 12000);
          });
      }
}
