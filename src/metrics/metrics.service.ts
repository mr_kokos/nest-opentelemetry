import { Injectable } from '@nestjs/common';
import { collectDefaultMetrics, Registry } from 'prom-client';

@Injectable()
export class MetricsService {
  private registry = new Registry();
  
  constructor() {
    collectDefaultMetrics({ register: this.registry });
  }

  getMetrics() {
    return this.registry.metrics();
  }
}