import { Module } from '@nestjs/common';
import { MetricsModule } from './metrics/metrics.module';
import { DelayModule } from './delay/delay.module';

@Module({
  imports: [MetricsModule, DelayModule],
  controllers: [],
  providers: [],
})
export class AppModule {}
